﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockTridiagonalisierung
{
    public class Scala
    {
        public double value;
        public string label;

        public Scala()
        {
            value = 0.0;
            label = "";
        }

        public Scala(double _value, string _label = "")
        {
            value = _value;
            label = _label;
        }

        public static Scala operator +(Scala s1, Scala s2)
        {
            double newVal = s1.value + s2.value;
            return new Scala(newVal);
            //return new Scala(newVal, s1.label + "+" + s2.label);
        }
        public static Scala operator -(Scala s1, Scala s2)
        {
            double newVal = s1.value - s2.value;
            return new Scala(newVal);
            //return new Scala(newVal, s1.label + "-" + s2.label);
        }
        public static Scala operator *(Scala s1, Scala s2)
        {
            double newVal = s1.value * s2.value;
            return new Scala(newVal);
            //return new Scala(newVal, s1.label + "*" + s2.label);
        }
        public static Scala operator /(Scala s1, Scala s2)
        {
            double newVal = s1.value / s2.value;
            return new Scala(newVal);
            //return new Scala(newVal, s1.label + "/" + s2.label);
        }

        public static  implicit operator Scala(double _value)
        {
            return new Scala(_value);
        }

        public Scala SquareRoot()
        {
            return new Scala(Math.Sqrt(value), @"\sqrt{"+label+"}");
        }

        public static implicit operator Scala(int i)
        {
            return new Scala(i);
        }

        public string PrintLatexString()
        {
            return label + "={ 0:N2}" + value.ToString();
        }

        public static bool operator ==(Scala s1, Scala s2)
        {
            return s1.value == s2.value;
        }
        public static bool operator !=(Scala s1, Scala s2)
        {
            return s1.value != s2.value;
        }
    }
}
