﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockTridiagonalisierung
{
    public class Blockmatrix
    {
        public int rows;
        public int columns;
        public Matrix[,] values;
        public string label;
        public int subRows;
        public int subColumns;

        private bool dirty = true;

        public Blockmatrix(int _rows, int _columns, int _subRows, int _subColumns, string _label = "")
        {
            rows = _rows;
            columns = _columns;
            values = new Matrix[rows, columns].Populate(() => new Matrix(_subRows, _subColumns));
            label = _label;
            subRows = _subRows;
            subColumns = _subColumns;
        }

        public Blockmatrix(Matrix[,] _values, string _label = "")
        {
            values = _values;
            rows = values.GetLength(0);
            columns = values.GetLength(1);
            label = _label;
            subRows = values[0, 0].rows;
            subColumns = values[0, 0].columns;
        }

        public double GetNorm()
        {
            double returnVal = 0.0;
            foreach (Matrix m in values)
                returnVal += m.GetNorm() * m.GetNorm();
            returnVal = Math.Sqrt(returnVal);
            return returnVal;
        }
        public void AddColumn(Blockvector _v, int _column)
        {
            if (_v.rows != columns)
                throw new Exception();
            for(int i = 0; i < _v.rows; i++)
            {
                values[i, _column] = _v.values[i];
            }
        }

        public void Prune(double _threshold)
        {
            for(int i = 0; i < rows; i++)
            {
                for(int j = 0; j < columns; j++)
                {
                    values[i, j].Prune(_threshold);
                }
            }
        }

        public Matrix ToMatrix()
        {
            Matrix returnMatrix = new Matrix(rows * subRows, columns * subColumns);
            for(int i = 0; i < rows; i++)
            {
                for(int j = 0; j < columns; j++)
                {
                    for(int k = 0; k < subRows; k++)
                    {
                        for(int l = 0; l < subColumns; l++)
                        {
                            returnMatrix.values[i * subRows + k, j * subColumns + l] = values[i, j].values[k, l];
                        }
                    }
                }
            }
            return returnMatrix;
        }

        public Blockmatrix Transpose()
        {
            Matrix[,] newVals = new Matrix[rows, columns];
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < columns; j++)
                    newVals[i, j] = values[j, i].Transpose();
            return new Blockmatrix(newVals);
        }

        public static Blockmatrix operator +(Blockmatrix m1, Blockmatrix m2)
        {
            if ((m1.rows != m2.rows) || (m1.columns != m2.columns))
                throw new Exception();
            Matrix[,] newVals = new Matrix[m1.rows, m2.columns];
            for (int i = 0; i < m1.rows; i++)
                for (int j = 0; j < m2.columns; j++)
                    newVals[i, j] = m1.values[i, j] + m2.values[i, j];
            return new Blockmatrix(newVals, m1.label + "+" + m2.label);
        }
        public static Blockmatrix operator -(Blockmatrix m1, Blockmatrix m2)
        {
            if ((m1.rows != m2.rows) || (m1.columns != m2.columns))
                throw new Exception();
            Matrix[,] newVals = new Matrix[m1.rows, m2.columns];
            for (int i = 0; i < m1.rows; i++)
                for (int j = 0; j < m2.columns; j++)
                    newVals[i, j] = m1.values[i, j] + m2.values[i, j];
            return new Blockmatrix(newVals, m1.label + "-" + m2.label);
        }
        public static Blockmatrix operator *(Blockmatrix m1, Blockmatrix m2)
        {
            if (m1.columns != m2.rows)
                throw new Exception();
            Matrix[,] newVals = new Matrix[m1.rows, m2.columns].Populate(() => new Matrix(m1.values[0, 0].values.GetLength(0), m1.values[0, 0].values.GetLength(1), ""));
            for (int i = 0; i < m1.rows; i++)
                for (int j = 0; j < m2.columns; j++)
                    for (int k = 0; k < m1.columns; k++)
                        newVals[i, j] += m1.values[i, k] * m2.values[k, j];
            return new Blockmatrix(newVals, m1.label + @" \cdot " + m2.label);
        }

        public string PrintLatexString(bool _onlyLabels = false)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"\underline{" + label + "}" + "=" + @"\left(\begin{matrix}");
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    sb.Append(values[i, j].PrintLatexString(_onlyLabels));
                    if (j < columns - 1)
                        sb.Append(@"&");
                }
                if (i < rows - 1)
                    sb.Append(@"\\");
            }
            sb.Append(@"\end{matrix}\right)");
            return sb.ToString();
        }

        public void PrintBitmap()
        {
            int subSize = values[0, 0].rows;
            Bitmap returnBitmap = new Bitmap(rows * subSize, columns * subSize);

            double max = 0;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    if (values[i, j].GetMax() > max)
                        max = values[i, j].GetMax();
                }
            }
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Matrix current = values[i, j];
                    for (int k = 0; k < current.rows; k++)
                    {
                        for (int l = 0; l < current.columns; l++)
                        {
                            int red = (int)((Math.Abs(current.values[k, l]) / max) * 765);
                            int green = red > 255 ? red - 255 : 0;
                            int blue = green > 255 ? green - 255: 0;
                            red = red > 255 ? 255 : red;
                            green = green > 255 ? 255 : green;
                            blue = blue > 255 ? 255 : blue;
                            returnBitmap.SetPixel(j * subSize + l, i * subSize + k, Color.FromArgb(red, green, blue));
                        }
                    }
                }
            }
            returnBitmap.Save(label + "_bitmap.png");
        }

        public string PrintJuliaString()
        {
            int subSize = values[0, 0].rows;
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            for (int i = 0; i < rows * subSize; i++)
            {
                for (int j = 0; j < columns * subSize; j++)
                {
                    int ci = i / subSize;
                    int cj = j / subSize;
                    sb.Append(values[ci, cj].values[i % subSize, j % subSize] + " ");
                }
                if (i < columns * subSize - 1)
                    sb.Append("; ");
            }
            sb.Append("]");
            return sb.ToString();
        }
    }
}
