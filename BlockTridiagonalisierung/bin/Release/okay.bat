rem batch.bat
rem %1 represents the file name with no extension.
pdflatex -jobname=output %1
convert -density 800 -alpha off output.pdf output.png
start output.png