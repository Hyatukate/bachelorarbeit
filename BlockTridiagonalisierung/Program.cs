﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlockTridiagonalisierung
{
    public class Program
    {
        public static ConsoleAppManager cam;
        private static string juliaProzessOutput;
        static void Main(string[] args)
        {
            InitializeJuliaProzess();
            
            double t = 0.25;
            double e = 1;
            double V = 0.5;
            int M = 40;
            double ef = 0.1;
            int k = 2;
            File.WriteAllText("input.tex",
@"\documentclass[preview,border=12pt]{standalone}
\usepackage{amsmath}
\usepackage{tikz}

\begin{document}
\setcounter{MaxMatrixCols}{50}");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            /*
            Matrix A = new Matrix(new double[,] {{ 1, 0.1 }, { 0.1, 1} }, "A");
            Matrix B = new Matrix(new double[,] { { 1, 0 }, { 0, 0 } }, "B");
            Matrix C = new Matrix(new double[,] { { 0, 0 }, { 0.1, 0 } }, "C");
            Matrix D = new Matrix(new double[,] { { 10, 0 }, { 0, 10 } }, "D");
            Matrix AB = A * B;
            Matrix CD = C * D;*/
            sw.Stop();
            /*Console.WriteLine("Operation Test Took: " + sw.ElapsedMilliseconds);
            string line = @"$$" + A.PrintLatexString() + " " + B.PrintLatexString() + " " + C.PrintLatexString() + " " + D.PrintLatexString() + "$$";
            File.AppendAllText("input.tex", line);
            line = @"$$" + AB.PrintLatexString() + " " + CD.PrintLatexString() + "$$";
            File.AppendAllText("input.tex", line);*/
            
            sw.Reset();
            sw.Start();
            /*Matrix H = Graph.BuildMatrixFromGraph(M, 2, ef, e, V, t, LatticeType.Chain);
            H.label = "H";
            Vector r0 = Graph.GetStartVector(M);
            Matrix T = Lanczos.Tridiagonalize(H, r0);
            T.label = "T";
            Console.WriteLine("One impurity: " + sw.ElapsedMilliseconds);
            sw.Stop();
            H.PrintBitmap();
            T.PrintBitmap();*/
            
            sw.Reset();
            sw.Start();
            Random rnd = new Random();
            //Blockmatrix BlockH = Graph.BuildBlockMatrixFromGraph(M, new int[] { (int)(rnd.NextDouble() * (M/2-1)  + 1.5), (int)(rnd.NextDouble() * (M/2 - 1) + 1.5) }, new double[] { ef, ef, ef }, e, V, t, LatticeType.Chain);
            Blockmatrix BlockH = Graph.BuildBlockMatrixFromGraph(M, new int[] { 778, 781 }, new double[] { ef, ef, ef, ef}, e, V, t, LatticeType.Triangle);
            BlockH.label = "BlockH";
            Blockvector BlockR0 = Graph.GetStartBlockVector(M, k, LatticeType.Square);
            Blockmatrix BlockT = Lanczos.Tridiagonalize(BlockH, BlockR0);
            BlockT.label = "BlockT";
            Console.WriteLine("Three impurities: " + sw.ElapsedMilliseconds);
            sw.Stop();
            BlockH.PrintBitmap();
            BlockT.PrintBitmap();
            Matrix endMatrix = BlockT.ToMatrix();
            List<string> localEnergies = new List<string>();
            List<List<string>> hoppingParameter = new List<List<string>>();
            for(int i = 0; i < k*2-1; i++)
            {
                hoppingParameter.Add(new List<string>());
            }
            for(int i = 0; i < endMatrix.rows; i++)
            {
                localEnergies.Add(i +  " " + endMatrix.values[i, i].ToString());
                for (int j = 1; j <= k*2-1; j++)
                {
                    if (i + j <= endMatrix.rows - 1)
                        hoppingParameter[j-1].Add(i + " " + endMatrix.values[i, i + j].ToString());
                    if(i - j >= 0)
                        hoppingParameter[j-1].Add(i + " " + endMatrix.values[i, i - j].ToString());
                }
            }
            File.WriteAllLines("LocalEnergies.txt", localEnergies);
            for (int i = 0; i < hoppingParameter.Count; i++)
                File.WriteAllLines("HoppingParameters_"+i+".txt", hoppingParameter[i]);
            // string line = @"$$" + BlockH.PrintLatexString(true) + "$$";
            // File.AppendAllText("input.tex", line);
            // line = @"$$" + BlockT.PrintLatexString(true) + "$$";
            // File.AppendAllText("input.tex", line);
            /*
            Console.ReadKey();
            *//*
            File.AppendAllText("input.tex",
@"\end{document}");

            Process p1 = new Process();

            p1.StartInfo.FileName = "okay.bat";
            p1.StartInfo.Arguments = "input";
            p1.StartInfo.UseShellExecute = false;

            p1.Start();
            p1.WaitForExit();*/

        }

        static void InitializeJuliaProzess()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");

            cam = new ConsoleAppManager("julia.exe");
            cam.StandartTextReceived += (source, line) => juliaProzessOutput += line;
            cam.ExecuteAsync();
            
            GetJuliaOutput("1+1");
            cam.Write("using RowEchelon");
        }

        public static string GetJuliaOutput(string _input, bool awaitOutput = false)
        {
            bool escape = false;
            bool first = true;
            while(!escape)
            {
                int wait = 0;
                cam.Write(_input);
                if(!first)
                    Console.WriteLine("Awaiting Julia output...");
                while (!escape && (wait < 200 || awaitOutput))
                {
                    if (juliaProzessOutput != null && juliaProzessOutput.EndsWith("\n"))
                        escape = true;
                    Thread.Sleep(10);
                    wait++;
                }
                first = false;
            }

            string returnString = juliaProzessOutput;
            juliaProzessOutput = null;
            return returnString;
        }
    }
}
