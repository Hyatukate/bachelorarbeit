﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockTridiagonalisierung
{
    public class Blockvector
    {
        public int rows;
        public int subRows;
        public int subColumns;
        public Matrix[] values;
        public string label;

        public Blockvector(int _length, string _label = "")
        {
            rows = _length;
            values = new Matrix[rows];
            label = _label;
        }

        public Blockvector(Matrix[] _values, string _label = "")
        {
            values = _values;
            rows = _values.Length;
            label = _label;
        }

        public static Blockvector operator +(Blockvector v1, Blockvector v2)
        {
            if (v1.rows != v2.rows)
                throw new Exception();
            Matrix[] newVals = new Matrix[v1.rows];
            for (int i = 0; i < v1.rows; i++)
                newVals[i] = v1.values[i] + v2.values[i];
            return new Blockvector(newVals);
            //return new Blockvector(newVals, v1.label + "+" + v2.label);
        }
        public static Blockvector operator -(Blockvector v1, Blockvector v2)
        {
            if (v1.rows != v2.rows)
                throw new Exception();
            Matrix[] newVals = new Matrix[v1.rows];
            for (int i = 0; i < v1.rows; i++)
                newVals[i] = v1.values[i] - v2.values[i];
            return new Blockvector(newVals);
            //return new Blockvector(newVals, v1.label + "-" + v2.label);
        }

        public static Matrix operator *(Blockvector v1, Blockvector v2)
        {
            if (v1.rows != v2.rows)
                throw new Exception();
            Matrix newVal = new Matrix(v1.values[0].rows, v1.values[0].columns);
            for (int i = 0; i < v1.rows; i++)
                newVal += v1.values[i] * v2.values[i];
            return newVal;
        }

        public static Matrix Dot(Blockvector v1, Blockvector v2)
        {
            if (v1.rows != v2.rows)
                throw new Exception();
            Matrix newVal = new Matrix(v1.values[0].rows, v1.values[0].columns);
            for (int i = 0; i < v1.rows; i++)
                newVal += v1.values[i].Transpose() * v2.values[i];
            return newVal;
        }

        public Matrix GetNorm()
        {
            return Blockvector.Dot(this,this).GetSquareRoot();
        }

        public Blockvector Transpose()
        {
            Matrix[] newVals = new Matrix[rows];
            for (int i = 0; i < rows; i++)
                newVals[i] = values[i].Transpose();
            return new Blockvector(newVals);
        }
        
        public void Prune(double _threshold)
        {
            double max = 0;
            for (int i = 0; i < rows; i++)
            {
                if (values[i].GetMax() > max)
                    max = values[i].GetMax();
            }
            for (int i = 0; i < rows; i++)
            {
                Matrix current = values[i];
                for(int j = 0; j < current.rows; j++)
                {
                    for(int k = 0; k < current.columns; k++)
                    {
                        if (Math.Abs(current.values[j, k] / max) < _threshold)
                            current.values[j, k] = 0;
                    }
                }
            }
        }

        public static Blockvector operator *(Blockvector v1, Matrix m1)
        {
            Matrix[] newVals = new Matrix[v1.rows];
            for (int i = 0; i < v1.rows; i++)
                newVals[i] = v1.values[i] * m1;
            return new Blockvector(newVals);
            //return new Blockvector(newVals, m1.label + " " + v1.label);
        }
        public static Blockvector operator *(Matrix m1, Blockvector v1)
        {
            Matrix[] newVals = new Matrix[v1.rows];
            for (int i = 0; i < v1.rows; i++)
                newVals[i] = m1 * v1.values[i];
            return new Blockvector(newVals);
            //return new Blockvector(newVals, m1.label + " " + v1.label);
        }
        public static Blockvector operator /(Blockvector v1, Matrix m1)
        {
            Matrix[] newVals = new Matrix[v1.rows];
            Matrix inverse = m1.Inverse();
            for (int i = 0; i < v1.rows; i++)
                newVals[i] = v1.values[i] * inverse;
            return new Blockvector(newVals);
            //return new Blockvector(newVals, v1.label + "/" + m1.label);
        }

        public static Blockvector operator *(Blockmatrix b1, Blockvector v1)
        {
            if (v1.rows != b1.rows)
                throw new Exception();
            int sizeSubMatrices = b1.values[0, 0].rows;
            Matrix[] newVals = new Matrix[v1.rows].Populate(() => new Matrix(sizeSubMatrices, sizeSubMatrices));
            for (int i = 0; i < b1.columns; i++)
                for (int j = 0; j < v1.rows; j++)
                    newVals[i] += b1.values[i, j] * v1.values[j];
            return new Blockvector(newVals);
            //return new Vector(newVals, m1.label + " " + v1.label);
        }
        public static Blockvector operator *(Blockvector v1, Blockmatrix b1)
        {
            if (v1.rows != b1.rows)
                throw new Exception();
            int sizeSubMatrices = b1.values[0, 0].rows;
            Matrix[] newVals = new Matrix[v1.rows].Populate(() => new Matrix(sizeSubMatrices, sizeSubMatrices));
            for (int i = 0; i < b1.columns; i++)
                for (int j = 0; j < v1.rows; j++)
                    newVals[i] += v1.values[j] * b1.values[j, i];
            return new Blockvector(newVals);
            //return new Vector(newVals, v1.label + " " + m1.label);
        }
        /*
        public Scala Dot(Vector v2)
        {
            if (rows != v2.rows)
                throw new Exception();
            Scala newVal = new Scala(0);
            for (int i = 0; i < rows; i++)
                newVal += values[i] * v2.values[i];
            return new Scala(newVal, label + @" \cdot " + v2.label);
        }
        */
        public string PrintLatexString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"\vec{" + label + "}" + "=" + @"\left(\begin{matrix}");
            for (int i = 0; i < rows; i++)
            {
                sb.Append(values[i].PrintLatexString());
                if (i < rows - 1)
                    sb.Append(@"\\");
            }
            sb.Append(@"\end{matrix}\right)");
            return sb.ToString();
        }

        public static bool operator ==(Blockvector b1, Blockvector b2)
        {
            if (b1.rows != b2.rows)
                return false;
            bool same = true;
            for (int i = 0; i < b1.rows; i++)
                if (b1.values[i] != b2.values[i])
                    same = false;
            return same;
        }

        // this is second one '!='
        public static bool operator !=(Blockvector b1, Blockvector b2)
        {
            if (b1.rows != b2.rows)
                return true;
            bool same = false;
            for (int i = 0; i < b1.rows; i++)
                if (b1.values[i] == b2.values[i])
                    same = true;
            return same;
        }
    }
}
