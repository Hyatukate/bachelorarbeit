﻿using System;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;

namespace BlockTridiagonalisierung
{
    public struct EigenStruct
    {
        public double[] eigenValues;
        public Vector[] eigenVectors;
    }

    public class Matrix
    {
        public int rows;
        public int columns;
        public double[,] values;
        public string label;

        private bool dirty = true;

        public Matrix(int _rows, int _columns, string _label = "")
        {
            rows = _rows;
            columns = _columns;
            values = new double[rows, columns].Populate(() => new double());
            label = _label;
        }

        public Matrix(double[,] _values, string _label = "")
        {
            values = _values;
            rows = values.GetLength(0);
            columns = values.GetLength(1);
            label = _label;
        }

        public static Matrix Identity(int _M)
        {
            Matrix returnMatrix = new Matrix(_M, _M);
            for (int i = 0; i < _M; i++)
                returnMatrix.values[i, i] = 1.0;
            return returnMatrix;
        }

        public static Matrix One(int _M)
        {
            Matrix returnMatrix = new Matrix(_M, _M);
            for (int i = 0; i < _M; i++)
                for(int j = 0; j < _M; j++)
                returnMatrix.values[i, j] = 1.0;
            return returnMatrix;
        }

        public EigenStruct GetEigenstruct()
        {
            //\[[^\[\]]*\]
            //[+-]?\d*\.\d*(e[+-]\d*)?
            StringBuilder sb = new StringBuilder();
            sb.Append("eig([");
            for (int j = 0; j < columns; j++)
            {
                for (int i = 0; i < rows; i++)
                {
                    sb.Append(values[i, j] + " ");
                }
                if (j < columns - 1)
                    sb.Append("; ");
            }
            sb.Append("])");
            Console.WriteLine(sb.ToString());
            string output = Program.GetJuliaOutput(sb.ToString());
            Console.WriteLine(output);
            MatchCollection matches = Regex.Matches(output, @"\[[^\[\]]*\]");
            string eigValues = matches[0].Value;
            string eigVectors = matches[1].Value;
            matches = Regex.Matches(eigValues, @"[+-]?\d*\.\d*(e[+-]\d*)?");
            int length = matches.Count;
            double[] eigenValues = new double[length];
            for (int i = 0; i < length; i++)
            {
                eigenValues[i] = double.Parse(matches[i].Value, CultureInfo.InvariantCulture);
            }
            matches = Regex.Matches(eigVectors, @"[+-]?\d*\.\d*(e[+-]\d*)?");
            Vector[] eigenVectors = new Vector[length];
            for (int i = 0, k = 0; k < matches.Count; i++, k += length)
            {
                Scala[] compoents = new Scala[length];
                for (int j = 0; j < length; j++)
                {
                    compoents[j] = double.Parse(matches[k + j].Value, CultureInfo.InvariantCulture);
                }
                eigenVectors[i] = new Vector(compoents);
            }
            EigenStruct eigenStruct = new EigenStruct();
            eigenStruct.eigenValues = eigenValues;
            eigenStruct.eigenVectors = eigenVectors;

            return eigenStruct;
        }
        public Matrix GetSquareroot()
        {
            if (CheckZero())
                return this;
            string output = Program.GetJuliaOutput("println(sqrtm(" + PrintJuliaString() + "))");
            return BuildMatrixFromJuliaOutput(output, rows, columns);
        }
        public bool CheckZero()
        {
            foreach (double value in values)
            {
                if (value != 0.0)
                    return false;
            }
            return true;
        }
        public Matrix Transpose()
        {
            Matrix returnMatrix = new Matrix(rows, columns);
            for(int i = 0; i < rows; i++)
            {
                for(int j = 0; j < columns; j++)
                {
                    returnMatrix.values[i, j] = values[j, i];
                }
            }
            return returnMatrix;
        }

        public void Prune(double _threshhold)
        {
            for(int i = 0; i < rows; i++)
            {
                for(int j = 0; j < columns; j++)
                {
                    if (values[i, j] < _threshhold)
                        values[i, j] = 0.0;
                }
            }
        }

        public double Det()
        {
            if (CheckZero())
                return 0.0;
            string output = Program.GetJuliaOutput("println(det(" + PrintJuliaString() + "))");            

            return double.Parse(output, CultureInfo.InvariantCulture);
        }

        public Matrix Inverse()
        {
            if (Det() == 0.0)
                return this;
            string s = PrintJuliaString();
            string output = Program.GetJuliaOutput("println(" + s + "^-1)");
            return BuildMatrixFromJuliaOutput(output, rows, columns);
        }

        public static Matrix BuildMatrixFromJuliaOutput(string _juliaOutput, int _rows, int _columns)
        {
            MatchCollection matches = Regex.Matches(_juliaOutput, @"[+-]?\d*\.\d*e?[+-]?\d");
            Matrix returnMatrix = new Matrix(_rows, _columns);

            for (int i = 0; i < _rows; i++)
            {
                for (int j = 0; j < _columns; j++)
                {
                    returnMatrix.values[i, j] = double.Parse(matches[i * _columns + j].Value, CultureInfo.InvariantCulture);
                }
            }

            return returnMatrix;
        }

        /*
        public EigenStruct GetEigenstruct()
        {
            Random rnd = new Random();
            string fileName = label != "" ? label+"_Matrix" : "#" + rnd.NextDouble() * int.MaxValue + "_Matrix";
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(rows + "d" + columns + "w");
            foreach (double s in values)
                stringBuilder.Append(s.value + "d");
            File.WriteAllText(fileName+".txt", stringBuilder.ToString());

            Process p1 = new Process();
            p1.StartInfo.FileName = "eigCSharpWrapper.bat";
            p1.StartInfo.Arguments = fileName;
            p1.StartInfo.UseShellExecute = false;

            p1.Start();
            p1.WaitForExit();

            string content = File.ReadAllText(fileName + "_Eig.txt");
            string[] parts = content.Split('w');
            List<string> eigenValueStrings = parts[0].Split('d').ToList();
            eigenValueStrings.Remove("");
            List<string> eigenVectorStrings = parts[1].Split('v').ToList();
            eigenVectorStrings.Remove("");
            double[] eigenValues = new double[eigenValueStrings.Count];
            Vector[] eigenVectors = new Vector[eigenVectorStrings.Count];
            for (int i = 0; i < eigenValueStrings.Count; i++)
            {
                eigenValues[i] = double.Parse(eigenValueStrings[i], CultureInfo.InvariantCulture);
            }
            for (int i = 0; i < eigenVectorStrings.Count; i++)
            {
                List<string> componentStrings = eigenVectorStrings[i].Split('d').ToList();
                componentStrings.Remove("");
                double[] components = new double[componentStrings.Count];
                for (int j = 0; j < componentStrings.Count; j++)
                {
                    components[j] = double.Parse(componentStrings[j], CultureInfo.InvariantCulture);
                }
                eigenVectors[i] = new Vector(components);
            }
            EigenStruct eigenStruct = new EigenStruct();
            eigenStruct.eigenValues = eigenValues;
            eigenStruct.eigenVectors = eigenVectors;

            return eigenStruct;
        }
        */
        /*
        public Matrix Inverse()
        {
            Random rnd = new Random();
            string fileName = label != "" ? label + "_Matrix" : "#" + rnd.NextDouble() * int.MaxValue + "_Matrix";
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(rows + "d" + columns + "w");
            foreach (double s in values)
                stringBuilder.Append(s.value + "d");
            File.WriteAllText(fileName + ".txt", stringBuilder.ToString());

            Process p1 = new Process();
            p1.StartInfo.FileName = "invCSharpWrapper.bat";
            p1.StartInfo.Arguments = fileName;
            p1.StartInfo.UseShellExecute = false;

            p1.Start();
            p1.WaitForExit();

            string content = File.ReadAllText(fileName + "_Inv.txt");
            string[] parts = content.Split('w');
            List<string> dimensions = parts[0].Split('d').ToList();
            dimensions.Remove("");
            List<string> newValues = parts[1].Split('d').ToList();
            newValues.Remove("");
            int newRows = int.Parse(dimensions[0]);
            int newColumns = int.Parse(dimensions[1]);
            Matrix returnMatrix = new Matrix(newRows, newColumns);
            for(int i = 0; i < newRows; i++)
            {
                for(int j = 0; j < newRows; j++)
                {
                    returnMatrix.values[i, j] = double.Parse(newValues[j + newColumns * i], CultureInfo.InvariantCulture);
                }
            }
            return returnMatrix;
        }*/

        public double GetMax()
        {
            return values.Cast<double>().Max();
        }

        public double GetNorm()
        {
            double returnVal = 0.0;
            foreach (double s in values)
                returnVal += (s * s);
            returnVal = Math.Sqrt(returnVal);
            return returnVal;
        }

        public Matrix GetSquareRoot()
        {
            return GetSquareroot();

            if (IsDiagonalMatrix())
            {
                Matrix d = new Matrix(rows, columns);
                for (int i = 0; i < rows; i++)
                {
                    d.values[i, i] = Math.Sqrt(values[i,i]);
                }

                return d;
            }
            else
            {
                EigenStruct eigenStruct = GetEigenstruct();
                int size = eigenStruct.eigenValues.Length;
                Matrix d = new Matrix(size, size);
                for (int i = 0; i < size; i++)
                {
                    d.values[i, i] = Math.Sqrt(eigenStruct.eigenValues[i]);
                }
                Matrix T = new Matrix(size, size);
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        T.values[i, j] = eigenStruct.eigenVectors[i].values[j].value;
                    }
                }

                return T * d * T;
            }
        }

        public bool IsDiagonalMatrix()
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    if (i != j && values[i, j] != 0)
                        return false;
                }
            }
            return true;
        }

        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            if ((m1.rows != m2.rows) || (m1.columns != m2.columns))
                throw new Exception();
            double[,] newVals= new double[m1.rows, m2.columns];
            for (int i = 0; i < m1.rows; i++)
                for (int j = 0; j < m2.columns; j++)
                    newVals[i, j] = m1.values[i, j] + m2.values[i, j];
            return new Matrix(newVals);
            //return new Matrix(newVals, m1.label + "+" + m2.label);
        }
        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            if ((m1.rows != m2.rows) || (m1.columns != m2.columns))
                throw new Exception();
            double[,] newVals = new double[m1.rows, m2.columns];
            for (int i = 0; i < m1.rows; i++)
                for (int j = 0; j < m2.columns; j++)
                    newVals[i, j] = m1.values[i, j] - m2.values[i, j];
            return new Matrix(newVals);
            //return new Matrix(newVals, m1.label + "-" + m2.label);
        }
        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            if (m1.CheckZero())
                return m1;
            if (m2.CheckZero())
                return m2;
            if (m1.columns != m2.rows)
                throw new Exception();
            double[,] newVals = new double[m1.rows, m2.columns];
            for (int i = 0; i < m1.rows; i++)
                for (int j = 0; j < m2.columns; j++)
                    for (int k = 0; k < m1.columns; k++)
                        newVals[i, j] += m1.values[i, k] * m2.values[k, j];
            return new Matrix(newVals);
            //return new Matrix(newVals, m1.label + @" \cdot " + m2.label);
        }
        public static Matrix operator *(Matrix m1, double s1)
        {
            double[,] newVals = new double[m1.rows, m1.columns].Populate(() => new double());
            for (int i = 0; i < m1.rows; i++)
                for (int j = 0; j < m1.columns; j++)
                    newVals[i, j] = m1.values[i,j] * s1;
            return new Matrix(newVals);
            //return new Matrix(newVals, m1.label + @" \cdot " + m2.label);
        }
        public static Matrix operator *(double s1, Matrix m1)
        {
            return m1 * s1;
            //return new Matrix(newVals, m1.label + @" \cdot " + m2.label);
        }

        public string PrintLatexString(bool _onlyLabels = false)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"\underline{"+label+"}" + "=" + @"\left(\begin{matrix}");
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    if(_onlyLabels)
                        sb.Append(values[i, j] != 0 ? 1: 0);
                    else
                        sb.Append(values[i, j]);
                    if (j < columns - 1)
                         sb.Append(@"&");
                }
                if(i < rows - 1)
                    sb.Append(@"\\");
            }
            sb.Append(@"\end{matrix}\right)");
            return sb.ToString();
        }

        public static bool operator ==(Matrix m1, Matrix m2)
        {
            if (m1.rows != m2.rows)
                return false;
            bool same = true;
            for (int i = 0; i < m1.rows; i++)
                for (int j = 0; j < m1.rows; j++)
                    if (m1.values[i,j] != m2.values[i,j])
                    same = false;
            return same;
        }

        // this is second one '!='
        public static bool operator !=(Matrix m1, Matrix m2)
        {
            if (m1.rows != m2.rows)
                return true;
            bool same = false;
            for (int i = 0; i < m1.rows; i++)
                for (int j = 0; j < m1.rows; j++)
                    if (m1.values[i,j] == m2.values[i,j])
                    same = true;
            return same;
        }

        public void PrintBitmap()
        {
            Bitmap returnBitmap = new Bitmap(rows, columns);
            for (int k = 0; k < rows; k++)
            {
                for (int l = 0; l < columns; l++)
                {
                    if (values[k, l] != 0.0)
                        returnBitmap.SetPixel(k, l, Color.Black);
                    else
                        returnBitmap.SetPixel(k, l, Color.White);
                }
            }
            returnBitmap.Save(label + "_bitmap.png");
        }

        public string PrintJuliaString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            for (int j = 0; j < columns; j++)
            {
                for (int i = 0; i < rows; i++)
                {
                    sb.Append(values[i, j] + " ");
                }
                if (j < columns - 1)
                    sb.Append("; ");
            }
            sb.Append("]");
            return sb.ToString();
        }
    }
}
