﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockTridiagonalisierung
{
    public class Node
    {
        public List<Node> targets;
        public int index;

        public Node(int _index)
        {
            index = _index;
            targets = new List<Node>();
        }
    }

    public enum LatticeType
    {
        Chain,
        Square,
        Hexagonal,
        Triangle,
        Random
    }

    public class Graph
    {
        public static double randomness = 0;

        public static Blockmatrix BuildBlockMatrixFromGraph(int _M, int[] _k, double[] _ef, double _e0, double _V, double _t, LatticeType _latticeType = LatticeType.Square)
        {
            Matrix baseMatrix = BuildMatrixFromGraph(_M, 0, 0, _e0, _V, _t, _latticeType);
            int sizeSubMatrices = _k.Length;
            int length = (baseMatrix.rows - 1) / sizeSubMatrices;
            Blockmatrix returnBlockMatrix = new Blockmatrix(length + 1, length + 1, sizeSubMatrices, sizeSubMatrices);

            Matrix identity = Matrix.Identity(sizeSubMatrices);
            Matrix one = Matrix.One(sizeSubMatrices);

            Matrix efMatrix = new Matrix(sizeSubMatrices, sizeSubMatrices);
            for (int i = 0; i < sizeSubMatrices; i++)
                efMatrix.values[i, i] = _ef[i];


            returnBlockMatrix.values[0, 0] = efMatrix;

            for(int i = 0; i < sizeSubMatrices; i++)
            {
                Matrix tmpMatrix = new Matrix(sizeSubMatrices, sizeSubMatrices);
                int index = _k[i] / sizeSubMatrices;
                int subIndex = _k[i] - index * sizeSubMatrices;
                tmpMatrix.values[subIndex, i] = _V;
                returnBlockMatrix.values[0, index + 1] += tmpMatrix.Transpose();
                returnBlockMatrix.values[index + 1, 0] += tmpMatrix;
            }

            for (int i = 1; i < returnBlockMatrix.rows; i++)
            {
                for (int j = 1; j < returnBlockMatrix.columns; j++)
                {
                    Matrix tmpMatrix = new Matrix(sizeSubMatrices, sizeSubMatrices);
                    for(int k = 0; k < sizeSubMatrices; k++)
                    {
                        for(int l = 0; l < sizeSubMatrices; l++)
                        {
                            tmpMatrix.values[k, l] = baseMatrix.values[(i-1)*sizeSubMatrices + k + 1, (j-1)*sizeSubMatrices + l + 1];
                        }
                    }
                    returnBlockMatrix.values[i, j] = tmpMatrix;
                }
            }

            return returnBlockMatrix;
        }

        public static Matrix BuildMatrixFromGraph(int _M, int _k, double _ef, double _e0, double _V, double _t, LatticeType _latticeType = LatticeType.Square)
        {
            List<Node> latticePoints = new List<Node>();

            switch(_latticeType)
            {
                case LatticeType.Chain:
                    latticePoints = GenerateChainLattice(_M, _k, _ef, _e0, _V, _t);
                    break;
                case LatticeType.Square:
                    latticePoints = GenerateSquareLattice(_M, _k, _ef, _e0, _V, _t);
                    break;
                case LatticeType.Hexagonal:
                    latticePoints = GenerateHexagonalLattice(_M, _k, _ef, _e0, _V, _t);
                    break;
                case LatticeType.Triangle:
                    latticePoints = GenerateTriangleLattice(_M, _k, _ef, _e0, _V, _t);
                    break;
                case LatticeType.Random:
                    latticePoints = GenearteRandomQuadLattice(_M, _k, _ef, _e0, _V, _t);
                    break;
            }

            int length = latticePoints.Count;
            Matrix returnMatrix = new Matrix(length + 1, length + 1);

            returnMatrix.values[0, 0] = _ef;
            returnMatrix.values[0, _k] = _V;
            returnMatrix.values[_k, 0] = _V;

            for (int i = 1; i < returnMatrix.rows; i++)
            {
                returnMatrix.values[i, i] = _e0;
            }

            foreach (Node parent in latticePoints)
            {
                foreach (Node child in parent.targets)
                {
                    returnMatrix.values[parent.index + 1, child.index + 1] = _t;
                }
            }

            return returnMatrix;
        }

        private static List<Node> GenearteRandomQuadLattice(int _M, int _k, double _ef, double _e0, double _V, double _t)
        {
            List<Node> latticePoints = new List<Node>();

            for (int i = 0; i < _M; i++)
            {
                for (int j = 0; j < _M; j++)
                {
                    latticePoints.Add(new Node(i + j * _M));
                }
            }

            for (int i = 0; i < _M; i++)
            {
                for (int j = 0; j < _M; j++)
                {
                    if (i != _M - 1)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M + 1]);
                    if (i != 0)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M - 1]);
                    if (j != _M - 1)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M + _M]);
                    if (j != 0)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M - _M]);
                }
            }

            Random rnd = new Random();


            foreach(Node latticePoint in latticePoints)
            {
                List<Node> targetsToRemove = new List<Node>();
                foreach (Node target in latticePoint.targets)
                {
                    if (rnd.NextDouble() < randomness)
                        targetsToRemove.Add(target);
                }

                foreach (Node target in targetsToRemove)
                {
                    target.targets.Remove(latticePoint);
                    latticePoint.targets.Remove(target);
                }
            }

            return latticePoints;
        }

        private static List<Node> GenerateTriangleLattice(int _M, int _k, double _ef, double _e0, double _V, double _t)
        {
            List<Node> latticePoints = new List<Node>();

            for (int i = 0; i < _M; i++)
            {
                for (int j = 0; j < _M; j++)
                {
                    latticePoints.Add(new Node(i + j * _M));
                }
            }

            for (int i = 0; i < _M; i++)
            {
                for (int j = 0; j < _M; j++)
                {
                    if (i != _M - 1)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M + 1]);
                    if (i != 0)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M - 1]);
                    if (j != _M - 1)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M + _M]);
                    if (j != 0)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M - _M]);
                    if(i != _M-1 && j != 0)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M - _M + 1]);
                }
            }

            return latticePoints;
        }

        private static List<Node> GenerateHexagonalLattice(int _M, int _k, double _ef, double _e0, double _V, double _t)
        {
            List<Node> latticePoints = new List<Node>();

            for (int i = 0; i < _M; i++)
            {
                for (int j = 0; j < _M; j++)
                {
                    latticePoints.Add(new Node(i + j * _M));
                }
            }

            for (int i = 0; i < _M; i++)
            {
                for (int j = 0; j < _M; j++)
                {
                    if(j % 2 == 0)
                    {
                        if(i % 2 == 0 && i != _M - 1)
                            latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M + 1]);
                        else if(i != 0)
                            latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M - 1]);
                    }
                    else
                    {
                        if (i % 2 != 0 && i != _M - 1)
                            latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M + 1]);
                        else if (i != 0)
                            latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M - 1]);
                    }
                    if (j != _M - 1)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M + _M]);
                    if (j != 0)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M - _M]);
                }
            }

            return latticePoints;
        }

        private static List<Node> GenerateSquareLattice(int _M, int _k, double _ef, double _e0, double _V, double _t)
        {
            List<Node> latticePoints = new List<Node>();

            for(int i = 0; i < _M; i++)
            {
                for(int j = 0; j < _M; j++)
                {
                    latticePoints.Add(new Node(i + j * _M));
                }
            }

            for (int i = 0; i < _M; i++)
            {
                for (int j = 0; j < _M; j++)
                {
                    if(i != _M - 1)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M + 1]);
                    if (i != 0)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M - 1]);
                    if (j != _M - 1)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M + _M]);
                    if (j != 0)
                        latticePoints[i + j * _M].targets.Add(latticePoints[i + j * _M - _M]);
                }
            }

            return latticePoints;
        }

        private static List<Node> GenerateChainLattice(int _M, int _k, double _ef, double _e0, double _V, double _t)
        {
            List<Node> latticePoints = new List<Node>();

            for (int i = 0; i < _M; i++)
            {
                latticePoints.Add(new Node(i));
            }

            for (int i = 0; i < _M; i++)
            {
                    if (i != _M - 1)
                        latticePoints[i].targets.Add(latticePoints[i + 1]);
                    if (i != 0)
                        latticePoints[i].targets.Add(latticePoints[i- 1]);
            }

            return latticePoints;
        }


        public static Vector GetStartVector(int _M)
        {
            Vector returnVector = new Vector(_M + 1);
            returnVector.values[0] = 1;
            for (int i = 1; i < returnVector.rows; i++)
                returnVector.values[i] = 0;
            return returnVector;
        }

        public static Blockvector GetStartBlockVector(int _M, int _sizeSubMatrices, LatticeType _latticeTpye = LatticeType.Square)
        {
            Blockvector returnBlockVector = null;
            switch(_latticeTpye)
            {
                case LatticeType.Chain:
                    returnBlockVector = new Blockvector(_M / _sizeSubMatrices + 1);
                break;
                case LatticeType.Square:
                case LatticeType.Hexagonal:
                case LatticeType.Triangle:
                case LatticeType.Random:
                    returnBlockVector = new Blockvector((_M * _M) / _sizeSubMatrices + 1);
                    break;
            }

            Matrix identity = Matrix.Identity(_sizeSubMatrices);
            returnBlockVector.values[0] = 1 * identity;
            for (int i = 1; i < returnBlockVector.rows; i++)
                returnBlockVector.values[i] = 0 * identity;
            return returnBlockVector;
        }
    }
}
