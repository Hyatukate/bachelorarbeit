﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockTridiagonalisierung
{
    public class Vector
    {
        public int rows;
        public Scala[] values;
        public string label;

        public Vector(int _length, string _label = "")
        {
            rows = _length;
            values = new Scala[rows];
            label = _label;
        }

        public Vector(Scala[] _values, string _label = "")
        {
            values = _values;
            rows = _values.Length;
            label = _label;
        }

        public Scala GetNorm()
        {
            Scala returnVal = new Scala(0.0);
            foreach (Scala s in values)
                returnVal += s*s;
            returnVal.value = Math.Sqrt(returnVal.value);
            return returnVal;
        }

        public static Vector operator +(Vector v1, Vector v2)
        {
            if (v1.rows != v2.rows)
                throw new Exception();
            Scala[] newVals = new Scala[v1.rows];
            for (int i = 0; i < v1.rows; i++)
                newVals[i] = v1.values[i] + v2.values[i];
            return new Vector(newVals);
            //return new Vector(newVals, v1.label + "+" + v2.label);
        }
        public static Vector operator -(Vector v1, Vector v2)
        {
            if (v1.rows != v2.rows)
                throw new Exception();
            Scala[] newVals = new Scala[v1.rows];
            for (int i = 0; i < v1.rows; i++)
                newVals[i] = v1.values[i] - v2.values[i];
            return new Vector(newVals);
            return new Vector(newVals, v1.label + "-" + v2.label);
        }

        public static Vector operator *(Scala s1, Vector v1)
        {
            return v1 * s1;
        }
        public static Vector operator *(Vector v1, Scala s1)
        {
            Scala[] newVals = new Scala[v1.rows].Populate(() => new Scala());
            for (int i = 0; i < v1.rows; i++)
                newVals[i] = v1.values[i] * s1;
            return new Vector(newVals);
            //return new Vector(newVals, s1.label + " " + v1.label);
        }
        public static Vector operator *(Matrix m1, Vector v1)
        {
            if (v1.rows != m1.rows)
                throw new Exception();
            Scala[] newVals = new Scala[v1.rows].Populate(() => new Scala());
            for (int i = 0; i < m1.columns; i++)
                for (int j = 0; j < v1.rows; j++)
                    newVals[i] += m1.values[i, j] * v1.values[j];
            return new Vector(newVals);
            //return new Vector(newVals, m1.label + " " + v1.label);
        }
        public static Vector operator *(Vector v1, Matrix m1)
        {
            if (v1.rows != m1.rows)
                throw new Exception();
            Scala[] newVals = new Scala[v1.rows].Populate(() => new Scala());
            for (int i = 0; i < m1.rows; i++)
                for (int j = 0; j < v1.rows; j++)
                    newVals[i] += m1.values[j, i] * v1.values[j];
            return new Vector(newVals);
            //return new Vector(newVals, v1.label + " " + m1.label);
        }
        public static Scala operator *(Vector v1, Vector v2)
        {
            if (v1.rows != v2.rows)
                throw new Exception();
            Scala newVal = new Scala(0);
            for (int i = 0; i < v1.rows; i++)
                newVal += v1.values[i] * v2.values[i];
            return new Scala(newVal.value);
            //return new Scala(newVal.value, v1.label + @" \cdot " + v2.label);
        }

        public static Vector operator /(Vector v1, Scala s1)
        {
            Scala[] newVals = new Scala[v1.rows];
            for (int i = 0; i < v1.rows; i++)
                newVals[i] = v1.values[i] / s1;
            return new Vector(newVals);
           // return new Vector(newVals, v1.label + "/" + s1.label);
        }

        public string PrintLatexString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"\vec{"+label+"}" + "=" + @"\left(\begin{matrix}");
            for (int i = 0; i < rows; i++)
            {
                sb.Append(values[i].value);
                if (i < rows - 1)
                    sb.Append(@"\\");
            }
            sb.Append(@"\end{matrix}\right)");
            return sb.ToString();
        }

        public static bool operator ==(Vector v1, Vector v2)
        {
            if (v1.rows != v2.rows)
                return false;
            bool same = true;
            for (int i = 0; i < v1.rows; i++)
                if (v1.values[i] != v2.values[i])
                    same = false;
            return same;
        }

        // this is second one '!='
        public static bool operator !=(Vector v1, Vector v2)
        {
            if (v1.rows != v2.rows)
                return true;
            bool same = false;
            for (int i = 0; i < v1.rows; i++)
                if (v1.values[i] == v2.values[i])
                    same = true;
            return same;
        }
    }
}
