﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockTridiagonalisierung
{
    class Lanczos
    {
        public static double pruning = 1.0e-4;

        //Take a Blockmatrix and tridiagonalize it via the startvector r0 and using the Lanczos-Algortihm
        //H  - Blockmatrix to diagonalize
        //r0 - Startvector for the Lanczos-Alogrithm
        public static Blockmatrix Tridiagonalize(Blockmatrix H, Blockvector r0)
        {
            //Initalize our orthonormalbasis
            List<Blockvector> basis = new List<Blockvector>();

            int sizeSubMatrices = H.values[0, 0].rows;
            
            //DEBUG//
            //Console.WriteLine(H.PrintJuliaString());
            
            //Initialize our transformation matrix
            Blockmatrix transformMatrix = new Blockmatrix(H.rows, H.columns, sizeSubMatrices, sizeSubMatrices);
            transformMatrix.label = "trans";

            basis.Add(r0);
            transformMatrix.AddColumn(r0, 0);

            //Frist vector of our basis
            Blockvector Hqn = H * r0;
            Matrix alpha = r0 * H * r0;
            Blockvector qn = Hqn - r0 * alpha;
            Matrix beta = qn.GetNorm();
            basis.Add(qn / beta);
            transformMatrix.AddColumn(basis[1], 1);
            
            
            for (int i = 1; i < H.rows - 1; i++)
            {
                //DEBUG
                //Console.WriteLine(i);

                //<n+1| = (T|n> - |n><n|T|n> - |n-1><n-1|T|n>) * \beta_0
                qn = H * basis[i] - basis[i] * ((basis[i].Transpose() * H) * basis[i]) - basis[i - 1] * ((basis[i - 1].Transpose() * H) * basis[i]);

                basis.Add(qn);
                basis[i + 1].Prune(pruning);        //Remove potential calculation erros
                beta = basis[i+1].GetNorm();

                //If norm == 0, default to a zero vector, every vector after this will subsequently be zero aswell
                if (beta.Det() == 0.0)
                    basis[i + 1] = new Blockvector(new Matrix[basis[i].rows].Populate(() => new Matrix(sizeSubMatrices, sizeSubMatrices)));
                else
                    basis[i + 1] = qn / beta;

                transformMatrix.AddColumn(basis[i + 1], i + 1);
            }

            //DEBUG//
            //transformMatrix.PrintBitmap();
            //Console.WriteLine(transformMatrix.PrintJuliaString());

            Blockmatrix returnMatrix = transformMatrix.Transpose() * H * transformMatrix;

            //DEBUG//
            //Console.WriteLine(returnMatrix.PrintJuliaString());


            //DEBUG
            //if (returnMatrix.rows <= 50)
            //{
            //    double aa;
            //    double.TryParse(Program.GetJuliaOutput(@"sum(eig(" + H.PrintJuliaString() + ")[1])", true), out aa);
            //    double bb;
            //    double.TryParse(Program.GetJuliaOutput(@"sum(eig(" + returnMatrix.PrintJuliaString() + ")[1])", true),  out bb);
            //    Console.WriteLine("a: " + aa + " b: " + bb + " Error: " + bb / aa);
            //    Console.ReadKey();
            //}            

            return returnMatrix;
        }

        //Used to save some computation time.
        public static List<Matrix> inverseScalarPorducts = new List<Matrix>();
        public static List<Blockvector> Reorthogonalize(List<Blockvector> _vecs)
        {
            int i = _vecs.Count - 1;
            List<Blockvector> returnList = _vecs.GetRange(0, i);
            Blockvector original = _vecs[i];
            Blockvector current = _vecs[i];
                for (int j = 0; j < i; j++)
                {
                    Matrix vw = Blockvector.Dot(_vecs[j], original);
                    if (j == inverseScalarPorducts.Count)
                    {
                        Matrix vv = Blockvector.Dot(_vecs[j], _vecs[j]);
                        inverseScalarPorducts.Add(vv.Inverse());
                    }
                    if (vw.CheckZero())
                        continue;
                    Matrix nn = vw * inverseScalarPorducts[j];
                    current -= nn * _vecs[j];
                }
            returnList.Add(current);
            return returnList;
        }

        //Reorthonormalize a given basis
        public static List<Blockvector> Reorthonormalize(List<Blockvector> _vecs)
        {
            int i = _vecs.Count - 1;
            //Make space for reorthonromalized vector
            List<Blockvector> returnList = _vecs.GetRange(0, i);
            Blockvector current = _vecs[i];
            for (int j = 0; j < i; j++)
            {
                Matrix vw = Blockvector.Dot(_vecs[j], current);
                current -= vw * _vecs[j];
            }
            current = current / current.GetNorm();
            returnList.Add(current);
            return returnList;
        }

        public static Matrix Tridiagonalize(Matrix H, Vector r0)
        {
            List<Vector> basis = new List<Vector>();
            List<Scala> alphas = new List<Scala>();
            List<Scala> betas = new List<Scala>();
            List<Vector> rs = new List<Vector>();

            basis.Add(new Vector(new Scala[H.rows].Populate(() => new Scala())));
            alphas.Add(0);
            rs.Add(r0);

            for (int i = 1; rs[i - 1] != basis[0]; i++)
            {
                betas.Add(rs[i - 1].GetNorm());
                basis.Add(rs[i - 1] / betas[i - 1]);
                rs.Add(H * basis[i]);
                alphas.Add(basis[i] * rs[i]);
                rs[i] = rs[i] - basis[i] * alphas[i] - basis[i - 1] * betas[i - 1];
            }

            Matrix returnMatrix = new Matrix(alphas.Count - 1, alphas.Count - 1, "T");
            for (int i = 1; i < alphas.Count - 1; i++)
            {
                returnMatrix.values[i - 1, i - 1] = alphas[i].value;
                returnMatrix.values[i, i - 1] = betas[i].value;
                returnMatrix.values[i - 1, i] = betas[i].value;
            }
            returnMatrix.values[returnMatrix.rows - 1, returnMatrix.columns - 1] = alphas[alphas.Count - 1].value;
            return returnMatrix;
        }

        private static bool CheckLinearity(Blockmatrix _b)
        {
            string input = "println(rref(" + _b.PrintJuliaString() + "))";
            string output = Program.GetJuliaOutput(input);
            Matrix checkMatrix = Matrix.BuildMatrixFromJuliaOutput(output, _b.rows * _b.subRows, _b.columns * _b.subColumns);
            checkMatrix -= Matrix.Identity(checkMatrix.rows);
            checkMatrix.Prune(1e-5);
            return checkMatrix.CheckZero();
        }

        public static int[] GetRandomIndizies(int _min, int _max, int _count)
        {
            Random rnd = new Random();
            int[] returnArray = new int[_count];
            for(int i = 0; i  < _count; i++)
            {
                returnArray[i] = (int)(rnd.NextDouble() * (_max - _min) + _min + 0.5);
            }
            return returnArray;
        }

        public static double[] GetRandomValues(double _min, double _max, int _count)
        {
            Random rnd = new Random();
            double[] returnArray = new double[_count];
            for (int i = 0; i < _count; i++)
            {
                returnArray[i] = rnd.NextDouble() * (_max - _min) + _min + 0.5;
            }
            return returnArray;
        }

        /*public static Matrix Tridiagonalize(Matrix m, Vector v)
        {
            return null;
            List<Vector> q = new List<Vector>();
            List<Vector> r = new List<Vector>();
            List<Scala> alpha = new List<Scala>();
            List<Scala> beta = new List<Scala>();
            Matrix H = m;

            alpha.Add(Alpha(H, v));
            beta.Add(v.GetNorm());
            q.Add(v);
            r.Add(v);
            Vector r1 = H * q[0] - Alpha(H, q[0]) * q[0];
            r.Add(r1);
            Vector q1 = r[1] / r[1].GetNorm();
            q.Add(q1);
            alpha.Add(Alpha(H, q[1]));
            beta.Add(r[1].GetNorm());

            for (int i = 2; i < 100; i++)
            {
                Vector ri = (H * q[i - 1] - alpha[i - 1] * q[i - 1] - beta[i - 2] * q[i - 2]);
                r.Add(ri);
                Vector qi =  r[i] / r[i-1].GetNorm();
                q.Add(qi);
                Scala alphai = Alpha(m, q[i]);
                alpha.Add(alphai);
                Scala betai = r[i].GetNorm();
                beta.Add(betai);
            }
        }

        private static Scala Alpha(Matrix m, Vector v)
        {
            return v * m * v;
        }*/
    }
}
